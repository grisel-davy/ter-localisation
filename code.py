import subprocess

"""
Code for gathering and analyzing the wifi networks in range;
Must be executed with root privileges.
"""

def scan():
    """ Scan in range wifi networks
    """

    subprocess.run(['wpa_cli','scan'])  # Scan the wifi

    results = subprocess.run(['wpa_cli','scan_results'], stdout = subprocess.PIPE).stdout.decode('utf-8')
    return(results)


def clean(results):
    """Results of the scan must be cleaned to be usable
    The infos that we need are:
        -BSSID that identify the beacon
        -Signal level that will be use for calculating the distance
    """
    
    lignes = results.split("\n")
    SSID = 'Cr@ns'

    for ligne in lignes:
        data = ligne.split('\t')
        if len(data)==5 and data[4] == SSID:
                print(ligne)

def distance(level):
    """Convert the signal level to a distance
    """
    pass

res = scan()

clean(res)

print("FIN")

    
    


