\documentclass[a4,12pt,titlepage]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage{mathtools}
\usepackage{tikz}
\usepackage{hyperref}
\usepackage{graphicx}
\graphicspath{{images/}}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
}

\usepackage[a4paper,top=3cm,bottom=2cm,left=2cm,right=2cm,marginparwidth=1.75cm]{geometry}

%VARIABLES====================================================
\def\myauthor{Pierre-Antoine Comby \& Arthur Grisel-Davy} % Author
\def\mytitle{Cartographie Wi-Fi en milieu inconnu} % title
\def\mysubtitle{TER format classique} % Subtitle
\def\mydate{Date} % date
%END VARIABLES================================================

\begin{document}

%TITLEPAGE====================================================
\begin{titlepage}
    \centering
    \vspace*{0.5 cm}
    \textsc{\LARGE \myauthor}\\[2.0 cm] % University Name
    \vspace{2cm}
    \rule{\linewidth}{0.2 mm} \\[0.4 cm]
    { \huge \bfseries \mytitle}\\
    \vspace{0.5cm}
    { \LARGE \bfseries \mysubtitle}\\
    \rule{\linewidth}{0.2 mm} \\[1.5 cm]

\begin{figure}[!tbp]
  \centering
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{images/logo_ens.png}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{images/logo_ups.png}
  \end{minipage}
\end{figure}
    
\end{titlepage}
%END TITLEPAGE=================================================

\begin{abstract}
Dans la plupart des bâtiments actuels, qu'ils soient domestiques ou
professionels, une bonne couverture Wi-Fi est attendue. Celle-ci
s'étend même de plus en plus aux espaces extérieurs. Le placement des
points d'accès pour parvenir à cette couverture optimale est donc une
question cruciale car ces émetteurs sont soumis à des problèmes
d'interférences, d'atténuations ou de rebonds autour des bâtiments. Pour
vérifier la couverture Wi-Fi d'un lieu, qu'il soit en extérieur ou
en intérieur, il faut donc mesurer la puissance de tous les réseaux à
chaque emplacement. Nous avons donc mis en place des outils pour
réaliser cette opération très fastidieuse. En utilisant au mieux les
différentes technologies de localisation dans les différents
environnements et les outils de visualisation appropriés, nous avons
développé un ensemble de méthodes pour cartographier le signal Wi-Fi en
extérieur comme en intérieur.
\end{abstract}

\tableofcontents

\newpage

\section{Présentation du sujet et problématique}
\label{sec:intro}

L'objectif de ce TER est de concevoir un outil fiable et précis de cartographie
des signaux Wi-Fi d'un lieu (dans notre cas le campus de l'ENS Paris-Saclay à Cachan).

Ainsi il faut pouvoir recenser à chaque position la puissance des différents
réseaux disponibles.

Parmi les applications d'une telle cartographie on peux nommer:
\begin{itemize}
  \item L'amélioration de la couverture Wi-Fi du campus.\\
  \item La géolocalisation (qui est alors le problème inverse: se repérer grâce aux signaux mesurés.)\\
\end{itemize}

Pour cela, un système de positionnement précis doit être utilisé.\\

En extérieur le GPS s'impose comme une référence, mais en intérieur il ne fournit plus de résultat cohérent (couverture de satellite inaccessible). De plus la précision des données GNSS est soumise aux aléas des couvertures satellitaires (arbres, bâtiments...). D'autre part, des capteurs relatifs (accéléromètre, magnétomètre, gyroscope) regroupés dans une centrale inertielle permettent d'obtenir des informations complémentaires sur les déplacements du système. Cependant ils sont eux aussi soumis à des limitations.\\

Les capteurs Wi-Fi , GPS , centrale inertielle sont quasi-systématiquement embarqués dans les smartphones actuels, ce qui rend l'utilisation de ces derniers facile d'accès et diffusable au plus grand nombre, ce qui permettrait de bénéficier de multiples sources de réception (crowd-sensing).

\section{État de l'art}

\subsection{IEEE802.11}
\label{subsec:wifi}

La norme IEEE802.11 est un ensemble de normes concernant les réseaux sans fils (dit Wi-Fi). Plusieurs protocoles ont été élaborés à partir de ces normes, améliorant le débit, et la portée des émetteurs.
Les protocoles utilisés dans nos expérimentations sont les variantes 802.11n et 802.11ac dont les caractéristiques sont rappelées dans le tableau ci-après:\\

\begin{tabular}{llll}
  Norme & Débit Binaire (Mbits)  & Portée intérieure (m) & Portée extérieure (m)\\
  \hline
  802.11n (2.4GHz)& 7.2 à 72  & 70 &  250 \\
  802.11n (5GHz)  & 15 à 150  & 12-35 & 250 \\
  802.11ac (5GHz) & 7.2 à  866 & 12-35 & N/A \\
\end{tabular}

Dans notre projet seule la portée des émetteurs nous importe, on ne cherche pas à se connecter à chaque point d'accès Wi-Fi.\\

Chaque réseau visible possède un identifiant simple, le SSID (Service Set Identifier), qui peux être diffusé par plusieurs bornes (comme ``eduspot'' ou ``Cr@ns''). Chaque point d'accès physique possède quand à lui un identifiant unique (similaire à une adresse MAC, sur 48bits) le BSSID (Basic Set Service Identifier).


\subsection{GPS}
\label{subsec:gps}
Le terme GPS (Global Positioning System) est couramment employé pour décrire tout système de positionemment par satellite. En réalité, un appareil se positionnant par GNSS (Global Navigation Satellite System) utilise de nombreux satellites qui appartiennent à des systèmes différents. En france un appareil classique captera le GPS (américain), le GLONASS (russe), le COMPASS (chinois) et même le GALILEO (européen) pour les appareils récents.
La technologie de positionnement par satellites est beaucoup utilisée pour la géolocalisation car elle présente généralement un coût assez faible pour une précision satisfaisante selon les cas d'utilisation. En extérieur, les précisions\footnote{La précision en m d'une mesure de position est le rayon du cercle dans lequel l'utilisateur a 68\% de chance de se trouver autour de la position mesurée} varient classiquement entre quelques mètres et plusieurs dizaines de mètres en fonction de l'environnement (immeubles, végétation dense). Cependant cette technologie présente également plusieurs inconvénients majeurs. D'une part la précision généralement supérieure au mètre ne convient pas à toute les applications. Un robot se déplaçant lentement en extérieur ne peut pas avoir une mesure de position fiable. D'autre part, ce système ne peut intrinsèquement pas être utilisé en intérieur où les ondes utilisées par les sattelites ne sont pas perçues.
\subsection{Centrale inertielle}
\label{subsec:IMU}
Les centrales inertielles sont des capteurs permettant de récupérer des informations sur les mouvements d'un objet (accélérations et vitesses angulaires) pour estimer la position et l'orientation. Elles sont généralement constituées d'un accéléromètre et d'un gyromètre permettant de recupérer des informations sur les six degrés de liberté du mouvement.

Elles sont quasi-systématiquement présentes dans les dispositifs embarqués et les smartphones, sous la forme de capteurs MEMS\footnote{Micro ElectroMechanical Sensors}. Cependant leur utilisation est limitée car très sensible à différents bruits (bruit thermique, électromagnétique) qui sont amplifiés par les différentes intégrations nécessaires pour estimer la position et l'orientation. 

\subsubsection{Accéléromètre et Gyromètre}

L'accéléromètre mesure les accélérations selon 3 axes orthogonaux. Il peut être de nature différente (piézoélectrique ou capacitif pour la plupart des dispositifs embarqués). La détection de l'accélération se fait via un système masse-ressort. Pour le gyroscope on utilise la force de Coriolis (avec un dispositif similaire à un pendule de torsion) comme sur la figure \ref{fig:accel}

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.4\textwidth]{mems-accelerometers}%
  \hfill
  \includegraphics[width=0.4\textwidth]{mems-gyro}
  \caption{Intérieur d'un accéléromètre (gauche) et gyromètre (droite)}
  \label{fig:accel}
\end{figure}

Comme le mobile est de taille microscopique, le bruit thermique est assez important, le capteur est également très sensible et mesure constamment le champ gravitationnel (projeté en fonction de l'orientation du mobile). Toutes ces pertubations génèrent, lors de l'intégration, une déviation systématique de la position, qu'il faut corriger. Il est donc nécessaire d'effectuer des traitement ultérieurs.

\subsubsection{Magnétomètre}

En plus de l'accéléromètre et du gyromètre les centrales inertielles peuvent inclure un magnétomètre, ce qui permet d'avoir une boussole (sur trois axes) ce qui permet de consolider (voire de remplacer) la mesure du gyromètre. Pour cela on mesure le champ magnétique terrestre de manière précise (valeur maximale de 60$\mu T$) via l'influence de la force de Lorentz. Ce capteur est également très sensible au bruit thermique auquel se rajoute le bruit geomagnétique de l'ordre de $0.1 nT$.

Une calibration précise, loin de toute interférence électromagnétique est nécessaire.

\subsubsection{Utilisation d'une centrale inertielle pour la localisation}
Dans une utilisation normale (capteur en main, dans une poche), les
pertubations induites par la marche doivent être modélisé pour estimer
la longueur et l'angle de pas
\cite{beauregard2006pedestrian}
faute de temps, le smartphone utilisé était fixé sur un chariot pour
pouvoir en controler les déplacements.

\subsection{Géolocalisation}
\subsubsection{Géolocalisation en extérieur}
La mesure de la couverture Wi-fi est un travail fastidieux mais nécessaire pour de nombreuses applications, comme la géolocalisation en intérieur se basant sur des méthodes de \emph{fingerprinting}\cite{he2015wi}
où l'on cherche à constituer une carte de la puissance du signal reçu (\emph{Receive Signal Strenght Indication}).
Constituer une telle carte est plutôt simple en extérieur en se basant sur les techniques de géolocalisation existantes. Le GPS offre une précision de 10 à 100 mètres pour les applications civiles
On peut également utiliser le réseau GSM/LTE qui, en agglomération,
profite de la redondance des antennes pour affiner la géolocalisation,
on retrouve alors une précision similaire à celle du GPS civil.
Différentes méthodes sont alors possibles: Angle of Arrival, Time
difference of Arrival, signature locale.  En pratique on fusionne les
différentes sources de géolocalisation (par filtre de Kalllan \cite{ristic2004beyond}
filtre particulaire \cite{klepal2008backtracking}
) ce qui permet d'obtenir une précision de l'ordre de 5m dans les meilleurs cas. Un autre moyen d'améliorer la géolocalisation est d'utiliser des informations a priori (murs, une voiture reste généralement sur la route...).

\subsubsection{Géolocalisation en intérieur}

En utilisant des technologies et capteurs il possible de concevoir des
systèmes de localisation dont les différentes solutions sont détaillées
dans \cite{farid2013recent}. Cependant sans vouloir utiliser de
matériel dédié et pour rendre la localisation accèssible au plus grand nombre il
faut se baser sur les différents champs de l'environnement (magnétique terrestre) ou, comme pour l'objectif final de ce TER, sur les émissions RF.

Sans carte préalable il est cependant possible d'utiliser un modèle de
propagation \cite{friis1971introduction}. Cependant nos mesures
(détaillées en \ref{subsec:wifi}) montrent que la mesure de puissance ne peut pas être un
un estimateur simple de la distance à la borne.


En parallèle des méthodes basées sur un couple émetteur récepteur, les
centrales inertielles très utilisées en aéoronique et navigation
sous-marine trouvent des applications dans la localisation en intérieur
relative à la dernière position connue/

\subsubsection{Construction de carte RSSI} 
La constitution de carte RSSI reste très fastidieuse, les travaux de
\cite{chintalapudi2010indoor} ou encore\cite{klepal2007influence} allègent la
constitution de cette carte en utilisant un modèle de propagation d'ondes, \cite{lee2012voronoi}
utilisent un graphe de Voronoï et \cite{liu2010indoor} construisent la carte de manière
collaborative . De telles méthodes n'ont pas pu être implémentées par manque de temps.

Découvertes un peu tard lors de ce TER , les méthodes
\emph{Simultaneous localization and mapping} sont complexes mais assez
prometteuses \cite{mirowski2013signalslam}.


\section{Technologies employées}
\subsection{Applications Android}
\label{subsec:app}
De nos jours, les téléphones portables grand public embarquent une multitude de capteurs. Tout les capteurs utiles pour notre application (GNSS, IMU, Carte Wi-Fi) sont par exemple présents dans n'importe quel smartphone du commerce. Cependant pour utiliser ces capteurs il est indispensable de construire des applications pouvant fonctionner sur le smartphone. Pour réaliser ces applications sans connaître le language de programmation d'Android, nous avons utilisé l'outil App Inventor 2 \cite{ref:ai2} développé par le MIT et fondé sur le language Scratch permettant de construire rapidement des applications à visée scientifique. Même si ce language impose quelques limitations pour l'exploitation et le traitement des données reçues, il permet cependant de récupérer rapidement les données des capteurs pour les traiter ultérieurement.\\

\subsection{Serveur et visualisation des données}
\label{subsec:serv}
Les données récupérées par l'application Android sont centralisées sur un serveur installé pour l'occasion. Nous avons utilisé le framework Django, pour la collecte et la gestion des données et leaflet et OpenStreetmap, pour la visualisation des données.

\paragraph{Django}
Django est un framework de développement web, open source, en Python. Il utilise une architechture MVT (Modèle, Vue, Template) et possède un ORM (Object Relation Manager) ce qui permet d'utiliser une base de données via l'instanciation d'objet python. Django possède de nombreuse applications développées par la communauté, permettant d'ajouter facilement des fonctionnalités évoluées au projet (interface d'administration, documentation, gestion de comptes... )


\paragraph{Leaflet}
LeafLet est une bibliothèque JavaScript de cartographie web qui
permet la visualisation de données sous la forme de couches \og
layers\fg{}. Elles possèdent également des extensions pour en étendre
le fonctionnement (carte de chaleur, clusterisation de points ...)

\section{Travail réalisé}
\label{sec:label}


\subsection{Étude du signal wifi}
\label{subsec:wifi}
Le signal Wi-Fi est à priori une bonne source d'information concernant la distance des bornes. Cependant ce signal est soumis à plusieurs phénomènes qui altèrent grandement sa qualité. 

\begin{itemize}
  \item Atténuation en espace libre. La puissance du signal Wi-Fi n'est pas linéaire avec la distance. Sa décroissance naturelle suit une courbe qui impose une décroissance très forte dans les premiers mètres (Figure \ref{fig:wifi-libre}). Avec une précision de mesure minimale de 1dB, il est alors impossible de différencier des signaux à plusieurs mètres d'écart.
  \item Interraction avec les murs. Lorsque le signal Wi-Fi rencontre un mur (ou tout autre surface), une partie est reflétée et une partie est transmise de l'autre coté avec une atténuation de la puissance.
  \item Diffraction. Le signal Wi-Fi est également soumis au phénomène de diffraction lors du passage par des ouvertures de quelques dizaines de centimètres.
\end{itemize}

\begin{figure}[h!]
\centering
\includegraphics[width=0.9\textwidth]{images/wifi-model.png}
\caption{Atténuation du signal Wi-Fi en espace libre \cite{ref:wifi-libre}}
\label{fig:wifi-libre}
\end{figure}

Pour vérifier la faisabilité d'un positionnement par Wi-Fi, nous avons réalisé des mesures dans une salle de cours. Cette salle possède un unique point d'accès Wi-Fi. Nous avons divisé la salle en 169 secteurs. Dans chaque secteur nous avons mesuré la puissance du signal reçue (avec un moyennage sur 5 mesures). Le résultat est présenté en Figure \ref{fig:heatmap-9g}. Ce résulat nous confirme qu'il est difficle de connaître la puissance du signal selon la distance à la borne et, réciproquement, qu'il est difficile de connaître la distance à la borne avec la puissance du signal. Tout ces éléments font que la puissance du signal Wi-Fi seule n'est pas une source fiable de positionnement en intérieur.

\subsubsection{Directionnalité des antennes réceptrices}
Lors de nos tests, il apparaissait que la qualité du signal Wi-Fi reçue dépendait de la direction de mesure. En visualisant la puissance du signal en direct sur un ordinateur avec un outil comme \href{https://github.com/uoaerg/wavemon}{wavemon}, nous pouvions détecter une variation de quelques décibels correspondant au changement de direction. Cela peut s'expliquer par la forme des antennes récéptrices dans les appareils. En effet si l'antenne est orientée selon une direction privilégiée, cela introduit des directions de réception privilégiées. Cependant il n'a pas été possible de trouver des résultats reproductibles pour conclure sur l'influence de la direction de mesure. Cela provient principalement du fait que les antennes ne sont généralement pas linéaires dans un ordinateur par exemple et donc sont capables de recevoir des signaux dans la plupart des directions sans différences majeures d'efficacité. Ce paramètre n'a donc pas été pris en compte dans le reste des mesures.

\begin{figure}[h!]
\centering
\includegraphics[width=0.8\textwidth]{images/heatmap_improved.png}
\caption{Carte de la puissance Wi-Fi (Point d'accès en jaune)}
\label{fig:heatmap-9g}
\end{figure}


\subsection{Étude de la centrale inertielle}
\label{subsec:imu_mesure}
Pour le positionement en intérieur il peut être approprié d'utiliser une centrale inertielle. Cependant ce type de capteur présente plusieurs types de défauts ou de particularités qu'il peut être difficile d'annuler. L'intérêt majeur de la centrale inertielle est qu'elle est présente dans tous les téléphones. Il est donc aisé de récupérer les données par une application. Les données recueillies sont généralement trés bruitées et biaisées. Nous avons donc appliqué plusieurs traitements pour analyser les données.\\


\begin{itemize}
  \item Calibration. Avant la prise de mesure, une étape de calibration est effectuée pour supprimer au maximum le biais initial sur les axes X et Y (plan du sol).\\
  \item Suppression de la moyenne. Étant donné les positions de départ et d'arrivée immobiles, la moyenne de l'accélération sur chacun des axes doit être nulle. Il est alors très efficace de supprimer cette moyenne à postériori. On constate alors que les vitesses déduites reviennent bien à zéro à la fin de l'acquisition.\\
  \item Utilisation d'un chariot. Pour diminuer le bruit, il est efficace de ne pas tenir le capteur dans la main en marchant. En effet la main est rarement à plat et les pas introduisent de très grandes accélérations qui perturbent les mesures. Pour cela l'utilisation d'un chariot sur lequel est fixé le capteur est très efficace car il augmente l'inertie et supprime les pas.\\
\end{itemize}

Aprés avoir efféctué tout ces traitements, on récupère des mesures du déplacement (Figures \ref{fig:caddie} et \ref{fig:kfet}).


\begin{figure}[h!]
\centering
\includegraphics[width=0.8\textwidth]{images/ligne_droite_caddie.png}
\caption{Mesure d'une ligne droite courte}
\label{fig:caddie}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[width=0.8\textwidth]{images/ligne_droite_kfet.png}
\caption{Mesure d'une ligne droite longue}
\label{fig:kfet}
\end{figure}

Sur le déplacement représenté en figure \ref{fig:caddie}, nous pouvons constater une très faible erreur dans la direction de déplacement. En effet le déplacement réel est de 3m et le déplacement mesuré est de 3.014m. Cependant une erreur apparait dans la direction perpendiculaire. Ce type d'erreur se retrouve sur le déplacement plus long de la figure \ref{fig:kfet}. Encore une fois le déplacement mesuré dans la direction du déplacement est précis mais une erreur apparait dans la direction perpendiculaire. Ces erreurs sont du à la planéité du sol sur lequel les mesures sont éfféctuées. En effet ce sol contient un creu en son centre et cela introduit une inclinaison du capteur qui n'est pas pris en compte par un accéléromètre seul. Cette faible inclinaison ne peut pas être corrigée car la calibration se fait sur l'emplacement de départ. Pour prendre en compte ces inclinaisons il est indispensable d'utiliser un second capteur.

\begin{itemize}
  \item Accéléromètre + Gyroscope. Ces deux capteurs ensemble peuvent théoriquement permettre de connaître l'inclinaison et tous les déplacements dans l'espace. Cependant le principe de fonctionnement du gyroscope est le même que l'accéléromètre et donc la déduction des angles utiles (pour déduire la direction de la gravité et l'annuler par exemple) présente de multiples sources d'erreur et de bruit. De plus les gyroscopes ne sont pas présents sur tous les téléphones. \\
  \item Accéléromètre + Magnétomètre. Ce troisième capteur peut aussi être utilisé pour déduire l'orientation plus directement qu'avec le gyroscope (pas d'intégration nécessaire). Il est possible de considérer le champ magnétique de la terre constant et ainsi de récupérer directement l'orientation par rapport au repère terrestre par la lecture du champ magnétique dans les trois directions de l'espace. Cependant le champ magnétique terrestre étant fiable, les bruits de mesure peuvent être très importants dans les environnements très metalliques (armoires, béton armé, etc). l'utilisation de ce capteur peut mener à l'implémentation d'un filtre de Kalman pour la fusion avec l'accéléromètre \cite{ref:magneto}. Ce capteur n'est pas non plus présent sur tous les téléphones.\\
\end{itemize}


\subsection{Partie Intérieure}
\label{sec:indoor}

En intérieur, le principe de la cartographie reste le même. Cette fois-ci, l'enjeu est d'obtenir une localisation précise. En effet il devient impossible d'utiliser la technologie GNSS en intérieur car les murs des bâtiments bloquent les ondes utilisées et engendrent des erreurs de mesures très importantes. Plusieurs autres technologies sont utilisés en intérieures:\\

\begin{itemize}
  \item Triangulation par signal Bluetooth: L'idée est de placer des émetteurs bluetooth dans une salle puis d'utiliser la puissance des signaux captée pour en déduire la distance à chaque émetteur et donc la position. L'inconvénient de cette solution est la mise en place du matériel dans toutes les salles à cartographier. De plus la réception et l'analyse des puissances Bluetooth n'est pas standard sur la plupart des appareils type smartphone.\\
  \item UltraWide Band. En utilisant des bandes de fréquence différentes, il est possible d'atteindre des précisions de l'odre du centimètre. Cependant cela nécéssite des appareils très spécifiques et donc complexes à utiliser.\\
  \item Triangulation par signal Wi-Fi. Sur le même principe que le Bluetooth, il est possible de mesurer les puissances des signaux Wi-Fi captées et d'en déduire une position. Cette solution est étudiée en partie \ref{subsec:wifi}.
  \item Utilisation d'une centrale inertielle. Avec une centrale inertielle, il est théoriquement possible de mesurer tous les déplacements. Cette solution est étudiée en partie \ref{subsec:imu_mesure}
\end{itemize}
\subsection{Partie Extérieure}
\label{sec:outdoor}
Pour effectuer une cartographie du réseau en extérieur, deux méthodes sont possibles. Il est possible d'utiliser un modèle de propagation des ondes Wi-Fi et l'emplacement des bornes pour estimer la couverture. Cependant ces modèles sont complexes et peu fiables. La solution que nous avons choisi est celle de la mesure directe de la puissance. Cette méthode a pour avantage de donner directement la couverture réelle. Cependant pour tout changement de configuration des points d'accès, il est nécéssaire de refaire les mesures.\\

Pour effectuer ces mesures, il est nécéssaire de maîtriser deux composantes. Dans un premier temps il faut connaître la position de mesure suffisamment précisément pour qu'elle soit valable. Pour cela on utilisera le capteur GNSS d'un téléphone portable. Dans un second temps il faut, pour chaque position, recueillir les différents réseaux Wi-Fi visibles et leurs différents paramètres (SSID, BSSID, puissance). En combinant ces données, il est possible de connaître la couverture sur toute une zone en extérieur.\\

Pour recueillir toutes ces données nous avons créé une application à l'aide de l'outil App Inventor 2 (\ref{subsec:app}). L'utilisation du capteur GNSS est intégrée à l'outil mais pour les réseau Wi-Fi nous avons utilisé l'extension TaifunWifi \cite{ref:taifunwifi}. Cet outil renvoie les SSID, BSSID et puissances de tout les réseaux visibles en quelques secondes.\\

L'application (Figure \ref{subsec:app}) est conçue pour être utilisée sans la participation active de l'utilisateur. Une fois lancée, l'application déterminera les changements de position et, pour toute nouvelle position, enregistrera les données Wi-Fi. Pour récupérer ces données, deux méthodes sont implémentées. Si le réseau Wi-FI est disponible et la connexion internet active, les données sont envoyées au serveur web par une requête GET. Si l'envoi n'est pas possible, les données sont stockées dans un fichier de log sur le téléphone. Après la prise de mesure, il suffit de récupérer le fichier de log et de le faire analyser par le serveur.

\begin{figure}[h!]
\centering
\includegraphics[width=0.3\textwidth]{images/app_gps_wifi.png}
\caption{Application de cartographie en extérieur}
\label{fig:app_gps}
\end{figure}



\subsubsection{Serveur Web}
Pour analyser et afficher les données recueillies, nous avons mis en place un site Web utilisant le framework Django.

Ce serveur utilise une base de données Posgresql et son extension GIS\footnote{Geographic Information System} pour l'adapter à des données géographiques. Les points collectés sont stockés dans une unique table selon le modèle Django suivant:

\begin{center}
  \begin{tabular}{lll}
    \bf Champs & \bf Type           & \bf Description                             \\
    \hline
    id         & int                & clé primaire                                \\
    bssid      & String(up to 17)   & BSSID du réseau au format XX:XX:XX:XX:XX:XX \\
    point      & Point\footnotemark & point de la mesure                          \\
    puissance  & int                & Puissance du signal mesuré en dBm           \\
    ssid       & string (up to 255) & SSID du réseau mesuré                       \\
    time       & Date(with time)    & Timestamp de l'instant de prise de mesure   \\
                             \hline
  \end{tabular}
\end{center}
\footnotetext{Objet fourni par l'extension GIS, stocke latitude et longitude}

\begin{figure}[h!]
\centering
\includegraphics[width=0.9\textwidth]{images/maps.png}
\caption{Affichage des mesures}
\label{fig:maps}
\end{figure}

Ces données sont ensuite affichées sur le Site Web à l'aide d'une vue django et d'un template HTML/ Javascript (pour la génération de la carte).
Pour un affichage optimal, les points proches sont regroupés en clusters pour accélérer l'affichage, il s'affichent en zoomant.
\begin{itemize}
\item Les données sont visualisables sur le site:
  \url{http://stark.adh.crans.org/maps/}.

\item Il est également possible de n'afficher qu'un seul réseau ou qu'une borne seule via son BSSID.
  \begin{itemize}
  \item \url{http://stark.adh.crans.org/maps/ssid/eduspot}
  \item \url{http://stark.adh.crans.org/maps/bssid/82:2a:a8:44:df:43}
  \end{itemize}
\end{itemize}


\paragraph{Analyse des mesures}

On remarque que les bornes ont une couverture et une portée très hétérogène, et les signaux captés en extérieur sont assez faibles, en effet les bornes placées majoritairement en intérieur voient leurs signaux fortement atténuées par les murs des batiments.

En moyenne on mesure une quinzaine de point d'accès différents à chaque instant, cependant une part d'entre-eux sont des points d'accès mobiles (téléphone en partage de connexion ou imprimantes ...), qui ne pourront pas être retenus pour les applications envisagées.
Cela implique également que les relevés vont varier dans le temps, non seulement en nombre de réseaux disponibles mais également sur les puissances de signaux mesurées. Il faut donc mettre à jour la carte régulièrement.

\newpage


\section{Conclusion}
\label{sec:conclusion}
L'objectif de cette étude était la création d'une carte représentant la qualité (assimilée à la puissance) du signal Wi-Fi dans toute une zone géographique (le campus dans notre cas). Nous voulions développer des outils et des méthodes pour permettre la création d'une telle carte facilement. Cette tâche s'est divisée en deux problèmes distincts que sont les parties extérieures et intérieures et avec des résultats différents.\\

La partie extérieure a demandé la création de nombreux outils pour la récolte, l'analyse et l'affichage des données. D'une part une application pouvant être installée par tous les utilisateurs sans connaissance préalable et permettant la récolte anonyme de données concernant la puissance Wi-Fi. Nous avons préféré une approche quantitative plutôt que qualitative pour chaque mesure car la précision intrinsèque de la localisation par GNSS ne nous permettait pas des mesures très précises. Certes, des améliorations sont encore à apporter à cette application (notamment en terme d'expérience utilisateur et de partage des données) mais elle remplit actuellement son rôle et fournit des données très pertinentes. L'affichage des données a elle aussi demandé un grand travail. Dans un premier temps la mise en place d'un serveur Web a permis la récupération et l'analyse des données en direct des applications connectées à internet. Ensuite ce serveur a servi d'hébergement pour le site web et son affichage interactif des données. Ce site est essentiellement composé de la carte mais pourrait être augmenté pour servir de portail principal pour les utilisateurs.\\

La partie intérieure a demandé une plus grande étude des capteurs et de leurs possibilités. Nous voulions créer un outil permettant une mesure simple et précise. Nous avons donc évité tous les modèles de propagation théoriques des signaux Wi-Fi qui, bien que potentiellement très précis, demandent une longue phase de création. Nous voulions un appareil capable de receuillir toutes les informations nécéssaires. Nous nous sommes heurtés au problème de la localisation précise en intérieur. Notre approche initiale utilisant les puissances des réseaux Wi-Fi s'est avérée impossible et nous avons obtenu des résultats confirmant l'inconsistance de ce paramètre. Nous nous sommes alors tournés vers les centrales inertielles et les différents capteurs annexes (gyroscope, magnétomètres) mais, faute de temps, nous n'avons pas pu mettre en place tous les traitements voulus.\\

Finalement, cette étude a été un succès pour la partie extérieure qui a rempli tous les objectifs que nous nous étions fixés. La partie intérieure quant à elle n'a pas mené au résultat attendu mais a permis une compréhension plus fine des phénomènes liés aux signaux Wi-Fi.

\newpage
\bibliographystyle{unsrt}
\bibliography{bibliographie}


\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
