# TER-Localisation

TER de M1 E3A. Localisation d'apareil en intérieure utilisant les bornes wifi.

## Taches réalisée:

Jour 1: Mercredi 15


* [X]  Premier test d'application pour récupérer la position GPS et les données Wifi avec App Inventor 2.
* [X]  Lecture de bibliographies, état de l'art (cf TER.org)

Jour 2: Jeudi 16

* [X] Récéption du matériel (Raspi0 ,cartes wifis, module GPS)
* [X] Premières utilisations du materiel. Vérification que tout est compatible. Initialisation des Raspi0.

Jour 3: Vendredi 17

* [X] Configuration d'un Serveur django pour le traitement des données. 
* [X] Amélioration de l'application de log Wifi/ GPS
* [X] Mise en forme des logs pour une utilisation sur OSM 
Jour 4: Samedi 18
* [X] Récupérations de l'application pour la récupération des logs GPS et Wifi en même temps
* [X] Création des fichiers d'affichage aux différents formats (kml, geojson)
Jour 5: Dimanche 19
* [ ] Réalisation d'une heatmap avec les données receuillies. Choix du logiciel (~~Debian maps~~, ~~openstreetmap~~, umaps?, geojson.io?).


Jour 20: Soutenance 

## TODO 

Traitement des données:
* Traitement des données GPS. La précision GPS à été améliorée à ~10m mais cela reste grand (on sait dans quelle 
rue on est mais pas de quel coté). Avec un modèle de bruit à priori (probablement additif gaussien centré) on devrait pouvoir améliorer cela.
* Traitement des données de signal. Pour l'instant seul le signal le plus fort est pris en compte mais tout les SSID/BSSID sont récupéré donc
d'autres analyses sont possibles.
